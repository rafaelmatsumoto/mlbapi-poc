const level = require("level");
const db = level("mlb-db");

let stream = db.createReadStream();

stream.on("data", (data) => {
  console.log("key=", data.key + ",value=" + data.value);
});
