# MLB Script

## Pre-requisites

- Node 12.19.0

## Installing

> npm i

## Running

- Get events:

> npm run start

- Query the database

> npm run db
