const fetch = require("node-fetch");
const crypto = require("crypto");
const level = require("level");
const db = level("mlb-db", { valueEncoding: "json" });

setInterval(() => {
  // Here we need to pass the MLB URL
  fetch(
    "https://statsapi.mlb.com/api/v1.1/game/635925/feed/live/diffPatch?language=en&startTimecode=20201021_002449"
  )
    .then((response) => response.json())
    .then((json) => {
      db.put(crypto.randomBytes(3).toString("hex"), json);
    });
}, 1000);
